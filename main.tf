// data collector is required for vRA Cloud, Optional for vRA 8.0
data "vra_data_collector" "dc" {
  count = var.datacollector != "" ? 1 : 0
  name  = var.datacollector
}


data "vra_region_enumeration_vsphere" "dc_regions" {
  for_each = var.vcenters
  username = each.value.vcenter_user
  password = var.vc_passwords[each.key]
  hostname = each.value.vcenter_name
  //regions  = []
  dcid = var.datacollector != "" ? data.vra_data_collector.dc[0].id : "" // Required for vRA Cloud, Optional for vRA 8.0
}


resource "vra_cloud_account_vsphere" "this" {
  depends_on              = [data.vra_region_enumeration_vsphere.dc_regions]
  for_each                = var.vcenters
  name                    = each.value.cloud_account_name
  description             = each.value.cloud_account_description
  username                = each.value.vcenter_user
  password                = lookup(var.vc_passwords, each.key)
  hostname                = each.value.vcenter_name
  //regions                = data.vra_region_enumeration_vsphere.dc_regions[each.key].regions
  regions                 = [ for val in each.value.datacenter.value:
                                val ]
  dcid                    = ""
  accept_self_signed_cert = true
  dynamic "tags" {
    for_each = each.value.tags
    content {
      key   = tags.key
      value = tags.value
    }
  }
}

output "vcregions" {
  value = data.vra_region_enumeration_vspheres.dc_regions
}

output "vmw_cloud_accounts" {
  value = vra_cloud_account_vsphere.this
}



//Outputs for logging
/*
//  see https://www.thegreatcodeadventure.com/building-dynamic-outputs-with-terraform-for_each-for-and-zipmap/ 
output "fred" {
  value = [
    for name, desc in zipmap(
      sort(keys(var.vcenters)),
    sort(values(vra_cloud_account_vsphere.this)[*]["description"])) :
    map("n", name, "f", desc)

  ]
}
*/

output "vcenters" {
  value = [
    for name, desc in zipmap(
      sort(keys(var.vcenters)),
    sort(values(vra_cloud_account_vsphere.this)[*]["name"])) :
    { "n" = name, "f" = desc, "q" = vra_cloud_account_vsphere.this[name].id, "x" = vra_cloud_account_vsphere.this[name].regions }

  ]
}



