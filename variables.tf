variable datacollector {
  default = ""
}

variable vcenters {
  type = map(object({
    vcenter_name              = string
    vcenter_user              = string
    cloud_account_name        = string
    cloud_account_description = string
    datacenters               = map(string)
    tags                      = map(string)
  }))
}

variable vc_passwords {
  type = map
  default = {
    "vc1" = "somemadeuppassword"
    "vc2" = "someothermadeuppassword"
  }
}
